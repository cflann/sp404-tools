import { Drive, Sample, SPImporter, SPExporter } from '../src/index'

const path = '/media/pi/SP-404SX'
const drive = new Drive(path)

const importer = new SPImporter(drive)
const exporter = new SPExporter(drive)

importer.importSamples()
  .then((samples) => {
    exporter.exportSamples(samples)
      .then(() => exporter.exportPadInfoBin(samples))
  })
