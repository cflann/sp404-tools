import fs from 'fs'
import path from 'path'

import Drive from './Drive'
import DriveStruct from './interfaces/DriveStruct'
import PadInfo from './PadInfo'
import Sample from './Sample'
import SampleStruct from './interfaces/SampleStruct'

export enum SPExporterOp {
  NOOP, CLEAR, WRITE, PAD_INFO_WRITE
}

export default class SPExporter {
  public drive: Drive

  constructor(drive: DriveStruct) {
    this.drive = Drive.fromStruct(drive)
  }

  private async writeSample(sample: Sample): Promise<SampleStruct> {
    const destPath = path.normalize(this.drive
      .samplePathFromIndex(sample.bankIndex, sample.padIndex))

    await sample.write(destPath)
    return sample.toStruct()
  }

  private async clearSample(sample: Sample): Promise<SampleStruct> {
    const destPath = path.normalize(this.drive
      .samplePathFromIndex(sample.bankIndex, sample.padIndex))

    try {
      // sync check for file existence, throws ENOENT if not
      fs.accessSync(destPath, fs.constants.F_OK)

      // async deletion
      await fs.promises.unlink(destPath)
    } catch (err) {
      // ENOENT means no sample file, this is expected unless sample is explicitly cleared
      if (err.code !== 'ENOENT') throw err
    } finally {
      // Return empty sample
      return {
        bankIndex: sample.bankIndex,
        padIndex: sample.padIndex,
        sourcePath: ''
      }
    }
  }

  async exportSamples(samples: SampleStruct[] = []): Promise<SampleStruct[]> {
    // Ensure sample op for all 120 pads; default to empty samples!
    let allSamples: Sample[] = new Array<Sample>(120)

    // Fill with given samples
    for (const struct of samples) {
      const sample = Sample.fromStruct(struct)
      const index = sample.absPadIndex
      allSamples[index] = sample
    }

    let results = new Array<SampleStruct>(120)

    // Initialize empty sample slots to default empty samples
    for (let i = 0; i < allSamples.length; i++) {
      const [ bankIndex, padIndex ] = PadInfo.indexFromLineNo(i)
      allSamples[i] = allSamples[i] || new Sample(bankIndex, padIndex)
      results[i] = allSamples[i].toStruct()
    }

    // Step 1: Perform samples writes and overwrites
    for (const struct of samples) {
      const sample = Sample.fromStruct(struct)
      if (!sample.isEmpty()) {
        results[sample.absPadIndex] = await this.writeSample(sample)
      }
    }

    // Step 2: Perform sample deletions
    for (const sample of allSamples) {
      if (sample.isEmpty()) {
        results[sample.absPadIndex] = await this.clearSample(sample)
      }
    }

    return Promise.all(results)
  }

  async exportPadInfoBin(samples: SampleStruct[]): Promise<any> {
    // Create array of PadInfo buffers with pad data, initially filled
    // with default data (defaults which would be auto-set by SP-404SX)
    let padInfoBuffs: Buffer[] = new Array<Buffer>(120)
    padInfoBuffs.fill((new PadInfo()).toBuffer())

    for (const struct of samples) {
      const sample = Sample.fromStruct(struct)
      const index = sample.absPadIndex
      const padInfo = await sample.getPadInfo()
      padInfoBuffs[index] = padInfo.toBuffer()
    }

    // Collect into single buffer and write to file
    const fileBuff = Buffer.concat(padInfoBuffs)
    return fs.promises.writeFile(this.drive.padInfoPath, fileBuff)
  }
}
