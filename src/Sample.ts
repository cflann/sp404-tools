import fs from 'fs'
import path from 'path'

import SampleStruct from './interfaces/SampleStruct'
import PadInfo from './PadInfo'
import { DEFAULTS } from './PadInfoFields'
import SPWaveFile from './SPWaveFile'

export default class Sample {
  // use this.getWaveFile() to access source!
  private source!: SPWaveFile

  constructor(
    public bankIndex: string, 
    public padIndex: number, 
    private _sourcePath: string = '',
    public origPadInfo?: PadInfo
  ) {}

  get sourcePath() {
    return this._sourcePath
  }

  /**
   * Can be used to shallow copy a sample 
   * (does not preserve imported pad info fields)
   */
  set sourcePath(newPath: string) {
    // Clear original pad info since sample is being overwritten
    this.origPadInfo = undefined
    this._sourcePath = path.normalize(newPath)
  }

  async getPadInfo(): Promise<PadInfo> {
    if (this.isEmpty()) return new PadInfo()

    const source = await this.getWaveFile()
    const sampleEnd = DEFAULTS.origSampleEnd + source.sampleLength

    // TODO: allow for custom "pad info" edits
    // Merge original pad info (if imported) with data from live source
    const origFields = this.origPadInfo ? this.origPadInfo.fields : {}
    let info = Object.assign({}, origFields, {
      origSampleEnd: sampleEnd,
      userSampleEnd: sampleEnd,
      channels: source.numChannels
    })
    return new PadInfo(info)
  }

  public get absPadIndex(): number {
    const bankIndexNo: number = this.bankIndex.charCodeAt(0) - 65
    return bankIndexNo * 12 + this.padIndex - 1 // 0 - 119
  }

  isEmpty() {
    return this.sourcePath === ''
  }

  /**
   * Returns duration of sample in seconds (in a promise)
   */
  async getDuration(): Promise<number> {
    if (this.isEmpty()) return 0
    const source = await this.getWaveFile()
    return source.duration
  }

  private async getWaveFile(): Promise<SPWaveFile> {
    if (this.isEmpty()) throw new Error('Cannot get WAV for empty sample.')
    if (this.source) return this.source
    const buff = await fs.promises.readFile(this.sourcePath)
    this.source = SPWaveFile.fromBuffer(buff)
    return this.source
  }

  /**
   * Deep copy, preserves any imported pad info fields
   */
  copy(bankIndex: string, padIndex: number): Sample {
    return new Sample(
      bankIndex || this.bankIndex,
      padIndex || this.padIndex,
      this.sourcePath,
      this.origPadInfo
    )
  }

  async write(destPath: string): Promise<any> {
    let spWaveFile = await this.getWaveFile()
    let write = spWaveFile.write(this.absPadIndex, destPath)

    const origPadInfo = this.origPadInfo
    this.sourcePath = destPath // update source path to new file
    this.origPadInfo = origPadInfo // BUT we don't want to lose orig PadInfo in this case

    return write
  }

  static fromStruct(struct: SampleStruct): Sample {
    return new Sample(
      struct.bankIndex,
      struct.padIndex,
      struct.sourcePath,
      struct.origPadInfo && PadInfo.fromStruct(struct.origPadInfo)
    )
  }

  toStruct(): SampleStruct {
    return {
      bankIndex: this.bankIndex,
      padIndex: this.padIndex,
      sourcePath: this.sourcePath,
      origPadInfo: this.origPadInfo && this.origPadInfo.toStruct()
    } as SampleStruct
  }
}
