import PadInfoFields from '../PadInfoFields'

export default interface PadInfoStruct {
  fields: PadInfoFields
}
