import PadInfoStruct from './PadInfoStruct'

export default interface SampleStruct {
  bankIndex: string
  padIndex: number
  sourcePath: string
  origPadInfo?: PadInfoStruct
}
