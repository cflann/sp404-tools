import { expect } from 'chai'
import path from 'path'

import Drive from '../src/Drive'
import DriveStruct from '../src/interfaces/DriveStruct'

describe('Drive', () => {
  const drivePath = '/media/user/SP-404SX'
  const smplDir = path.join('ROLAND', 'SP-404SX', 'SMPL')
  let drive = new Drive(drivePath)

  describe('padInfoPath()', () => {
    it('should return the standard path to PAD_INFO.BIN', () => {
      expect(drive.padInfoPath).to.equal(
        path.join(drivePath, smplDir, 'PAD_INFO.BIN')
      )
    })
  })

  describe('samplePathFromIndex()', () => {
    it('should check for bad bank index and throw', () => {
      const bankIndex = 'Z' // bad index
      expect(() => drive.samplePathFromIndex(bankIndex, 1))
        .to.throw('Invalid bank')
    })

    it('should check for bad pad index and throw', () => {
      const padIndex = 666 // bad index
      expect(() => drive.samplePathFromIndex('A', padIndex))
        .to.throw('Invalid pad')
    })

    it('should return standard sample path', () => {
      expect(drive.samplePathFromIndex('A', 1))
        .to.equal(path.join(drivePath, smplDir, 'A0000001.WAV'))
      expect(drive.samplePathFromIndex('C', 11))
        .to.equal(path.join(drivePath, smplDir, 'C0000011.WAV'))
    })
  })

  describe('fromStruct()', () => {
    it('should preserve fields', () => {
      const struct: DriveStruct = {
        path: '/test/drive/path'
      }

      const drive: Drive = Drive.fromStruct(struct)
      expect(drive.path).to.equal(struct.path)
    })
  })

  describe('toStruct()', () => {
    it('should preserve fields', () => {
      const drive: Drive = new Drive('/test/drive/path')
      const struct: DriveStruct = drive.toStruct()
      expect(struct.path).to.equal(drive.path)
    })
  })
})
