import { expect } from 'chai'
import mock from 'mock-fs'
import fs from 'fs'
import path from 'path'

import SPWaveFile from '../src/SPWaveFile'

describe('SPWaveFile', () => {
  let wavBuff: Buffer

  before(async () => {
    mock({
      // @ts-ignore
      '48k32bit.WAV': mock.load(path.resolve(__dirname, 'data/48k32bit.WAV'), { lazy: false })
    })
    wavBuff = await fs.promises.readFile('48k32bit.WAV')
  })

  describe('fromBuffer()', () => {
    it('should ensure SP-404 level fidelity', async () => {
      let spWav = SPWaveFile.fromBuffer(wavBuff)
      expect(spWav.sampleRate).to.equal(44100)
      expect(spWav.bitDepth).to.equal(16)
    }).timeout(3000)
  })

  describe('write()', async () => {
    it('should write the proper wav header', async () => {
      let spWav = SPWaveFile.fromBuffer(wavBuff)
      const dest = 'A0000010.WAV' // absolute pad index: 9
      spWav.write(9, dest)
      expect(() => fs.accessSync(dest, fs.constants.F_OK)).to.not.throw()
      let newBuff = await fs.promises.readFile(dest)
      
      const RIFF = newBuff.slice(0, 4)
      expect(RIFF.toString('hex')).to.equal('52494646')
      const FMT = newBuff.slice(12, 38)
      expect(FMT.toString('hex'))
        .to.equal('666d7420120000000100020044ac000010b10200040010000000')
      const padIndex = newBuff[58]
      const DATA = newBuff.slice(504, 508)
      expect(DATA.toString('hex')).to.equal('64617461')
      const dataLength = newBuff.length - 512
      expect(dataLength).to.equal(spWav.sampleLength)
    }).timeout(3000)
  })

  after(mock.restore)
})
