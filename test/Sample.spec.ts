import { expect } from 'chai'
import fs from 'fs'
import path from 'path'

import Drive from '../src/Drive'
import PadInfo from '../src/PadInfo'
import { DEFAULTS as PAD_INFO_DEFAULTS } from '../src/PadInfoFields'
import Sample from '../src/Sample'
import SampleStruct from '../src/interfaces/SampleStruct'

describe('Sample', () => {

  describe('getPadInfo()', () => {
    it('should return default for empty pad', async () => {
      const sample = new Sample('A', 1, '')
      const padInfo = await sample.getPadInfo()
      expect(padInfo).to.deep.equal(new PadInfo())
    })
  })

  describe('absPadIndex()', () => {
    it('should return 0 for A1, 119 for J12', () => {
      const sample1 = new Sample('A', 1, '')
      expect(sample1.absPadIndex).to.equal(0)
      const sample2 = new Sample('J', 12, '')
      expect(sample2.absPadIndex).to.equal(119)
    })
  })

  describe('copy()', () => {
    it('should preserve source path and imported fields', async () => {
      const sPath = path.resolve(__dirname, 'data/A0000001.WAV')
      const origPadInfo = new PadInfo({
        volume: 100,
        lofi: true,
        reverse: true
      })
      const sample = new Sample('A', 1, sPath, origPadInfo)
      const padInfo = await sample.getPadInfo()

      expect(padInfo.fields.volume).to.equal(100)
      expect(padInfo.fields.lofi).to.be.true
      expect(padInfo.fields.reverse).to.be.true
    })
  })

  describe('write()', () => {
    it('should update sourcePath to newly saved file', async () => {
      const drive = new Drive(path.resolve(__dirname, 'data/SP-404SX'))
      const sPath = path.resolve(__dirname, 'data/A0000001.WAV')
      const dPath = drive.samplePathFromIndex('A', 1)
      const sample = new Sample('A', 1, sPath)
      await sample.write(dPath)
      expect(sample.sourcePath).to.equal(dPath)
      await fs.promises.unlink(dPath)
    })
  })

  describe('fromStruct()', () => {
    it('should preserve fields', () => {
      const struct: SampleStruct = {
        bankIndex: 'A',
        padIndex: 1,
        sourcePath: '/test/path/to/sample.wav',
        origPadInfo: {
          fields: PAD_INFO_DEFAULTS
        }
      }

      const sample: Sample = Sample.fromStruct(struct)
      expect(sample.bankIndex).to.equal(struct.bankIndex)
      expect(sample.padIndex).to.equal(struct.padIndex)
      expect(sample.sourcePath).to.equal(struct.sourcePath)
      const sampleFields = sample.origPadInfo && sample.origPadInfo.fields || {}
      expect(sampleFields).to.deep.equal(struct.origPadInfo!.fields)
    })
  })

  describe('toStruct()', () => {
    it('should preserve fields', () => {
      const sample: Sample = new Sample('A', 1, '/test/path/to/sample.wav')
      const struct: SampleStruct = sample.toStruct()
      expect(sample.bankIndex).to.equal(struct.bankIndex)
      expect(sample.padIndex).to.equal(struct.padIndex)
      expect(sample.sourcePath).to.equal(struct.sourcePath)
      expect(struct.origPadInfo).to.be.undefined
    })
  })
})
