import { expect } from 'chai'
import range from 'lodash/range'

import { BankIndexValidator, PadIndexValidator } from '../src/Validation'

describe('BankIndexValidator', () => {
  describe('isValid()', () => {

    it('should return true for A-J', () => {
      'ABCDEFGHIJ'.split('').forEach(letter => {
        expect(BankIndexValidator.isValid(letter)).to.be.true
      })
    })

    it('should return false for other values', () => {
      'abcKLM123'.split('').forEach(letter => {
        expect(BankIndexValidator.isValid(letter)).to.be.false
      })
      expect(BankIndexValidator.isValid('ABC')).to.be.false
    })
  })
})

describe('PadIndexValidator', () => {
  describe('isValid()', () => {

    it('should return true for 1-12', () => {
      range(1, 13).forEach(index => {
        expect(PadIndexValidator.isValid(index)).to.be.true
      })
    })

    it('should return false for other values', () => {
      expect(PadIndexValidator.isValid(0)).to.be.false
      expect(PadIndexValidator.isValid(1.5)).to.be.false
      expect(PadIndexValidator.isValid(13)).to.be.false
    })
  })
})
